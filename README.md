# vajehyab

Vajehyab.ir Simple Bash Script







### Installation requierments:

```bash
sudo apt-get install curl jq
```

1. Copy API from this [link](https://www.vajehyab.com/i/developer).
2. Open `vajehyab.sh`.
3. Replace your own API with `API_KEY`.
```bash
api=API_KEY
```

```bash
git clone https://gitlab.com/strusvos/vajehyab.git && cd vajehyab
chmod +x ./vajehyab.sh
```

#### Run the script:

```bash
./vajehyab.sh میهن
```
